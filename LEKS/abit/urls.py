from django.urls import path
from django.conf.urls import url

from . import views
app_name = 'abit'

urlpatterns = [
	path('mylist/', views.usermaths.as_view(), name='my-abits'),
	url(r'^$', views.index, name='index'),
    url(r'^(?P<abitkey>[^/]+)/$', views.AbitDetailView.as_view(), name='abit-detail'),
]