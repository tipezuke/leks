from channels.generic.websocket import AsyncWebsocketConsumer
from .models import MathInstance, MathPage
from channels.db import database_sync_to_async
import json
import logging

class MathInstanceConsumer(AsyncWebsocketConsumer):
	async def connect(self):
		self.abitkey = self.scope['url_route']['kwargs']['abitkey']
		self.instance_index = 'page_%s' % self.abitkey

		

		await self.accept()

	async def receive(self, text_data):
		self.set_data(text_data)

	def set_data(self, text_data):
		text_data_json = json.loads(text_data)
		pageindex = int(text_data_json['index'])
		message = text_data_json['message']

		mathi = MathInstance.objects.get(pk=self.abitkey)
		pages = MathPage.objects.filter(instance=mathi)
		page = pages.get(index=pageindex)
		page.content = message
		page.save()