from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.urls import reverse
from django.http import HttpResponseRedirect

class MathInstance(models.Model):
	owner = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
	title = models.CharField(max_length=100)
	created_date = models.DateTimeField(default=timezone.now())

class MathPage(models.Model):
	instance = models.ForeignKey(MathInstance, on_delete=models.CASCADE)
	content = models.CharField(max_length=100000)
	index = models.IntegerField(default=0)