from django.urls import path

from . import views
app_name = 'users'

urlpatterns = [
    path('new', views.signup, name='user_new'),
]