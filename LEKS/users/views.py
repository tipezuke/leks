from django.shortcuts import render

from django.http import HttpResponse
from django.template import loader
from django.shortcuts import redirect
from django.utils import timezone

from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from django.contrib.auth.models import Group

def signup(request):
	if request.method == 'POST':
		form = UserCreationForm(request.POST)
		if form.is_valid():
			form.save()
			username = form.cleaned_data.get('username')
			raw_password = form.cleaned_data.get('password1')

			user = User.objects.create_user(username, raw_password)
			my_group = Group.objects.get('basic')
			my_group.user_set.add(user)

			login(request, user)
			return redirect('/users/')
	else:
		form = UserCreationForm()
		return render(request, 'users/signup.html', {'form': form})