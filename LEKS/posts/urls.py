from django.urls import path

from . import views
app_name = 'posts'

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:submission_id>/', views.details, name='detail'),
    path('new', views.createpost, name='post_new'),
]