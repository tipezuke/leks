from django.shortcuts import render

from django.http import HttpResponse
from django.template import loader

from .models import Submission

from .forms import SubmissionForm
from django.shortcuts import redirect
from django.utils import timezone

def index(request):
	latest_submission_list = Submission.objects.order_by('-pub_date')[:500]
	context = {
		'latest_submission_list': latest_submission_list,
		}
	return render(request, 'posts/index.html', context)

def details(request, submission_id):
	if request.method == "POST":
		if request.POST.get('delete'):
			Submission.objects.get(pk=submission_id).delete()
			return redirect('/posts')
	else:
		try:
			submission = Submission.objects.get(pk=submission_id)
		except Submission.DoesNotExist:
			raise Http404("Submission does not exist")
		return render(request, 'posts/detail.html', {'submission': submission.submission_text})

def createpost(request):
	if request.method == "POST":
		form = SubmissionForm(request.POST)
		if form.is_valid():
			data = request.POST.dict()
			submission = Submission()
			submission.submission_text = data.get('text')
			submission.submission_title = data.get('title')
			submission.pub_date = timezone.localtime()
			submission.save()
		return redirect('/posts/' + str(submission.id))
	else:
		form = SubmissionForm()
	return render(request, 'posts/createpost.html', {'form': form})

