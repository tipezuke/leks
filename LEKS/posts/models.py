from django.db import models

class Submission(models.Model):
    submission_title = models.CharField(max_length=200)
    submission_text = models.CharField(max_length=1000)
    pub_date = models.DateTimeField('date published')
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.submission_title